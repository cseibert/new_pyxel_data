# Pyxel Data

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?urlpath=lab)

This repository contains example notebooks for Pyxel and data that would clutter the main Pyxel repository. This repository can be downloaded and examples can be run locally.

They are also available on Binder, without prior Pyxel installation, by clicking on the badge above.

## Clone Pyxel Data

Before to clone Pyxel Data you **must** install the extension Git LFS.

:fire: If this extension is not installed, the cloned code
will be not operational ! :fire:

See this [guide](https://git-lfs.github.com) on how to install Git LFS.

Instructions to clone Pyxel Data:

```bash
git lfs install
git clone https://gitlab.com/esa/pyxel-data.git
cd pyxel-data
jupyter lab
```

## Download Pyxel Data

The preferred way to get Pyxel data is to download it.

### Download manually

You can download Pyxel Data directly as a zip file by clicking the '[download](https://gitlab.com/esa/pyxel-data/-/archive/master/pyxel-data-master.zip)' link next to the 'clone' link.

### Download via Pyxel

If Pyxel is already installed, you can download Pyxel Data with the following command:

```bash
pyxel download-examples
cd pyxel-examples
jupyter lab
```

## Links

[![docs](https://esa.gitlab.io/pyxel/documentation.svg)](https://esa.gitlab.io/pyxel/doc)
[![gitter](https://badges.gitter.im/pyxel-framework/community.svg)](https://gitter.im/pyxel-framework/community)
[![Google Group](https://img.shields.io/badge/Google%20Group-Pyxel%20Detector%20Framework-blue.svg)](https://groups.google.com/g/pyxel-detector-framework)
[![doi](https://zenodo.org/badge/DOI/10.1117/12.2314047.svg)](https://doi.org/10.1117/12.2314047)
